package com.actitimeframework.www.ActitimeSeleniumFramework;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

// click, right click, click & hold, release, mouse over, Right clicK& keyboard actions 
public class MouseActions extends ExcelUtils {
	//public void mouseClick(WebElement element, WebDriver driver){
	public void mouseClick(WebElement element){
	// create an object from the Selenium "Actions" class
		Actions mouseActions = new Actions(driver);
		mouseActions.click(element);
		// Imp Note: build.perform must be executed after each mouse action to perfrom the mouse operation
		mouseActions.build().perform();
	}
	
	public void mouseRightClick(WebElement element){
		
		// create an object from the Selenium "Actions" class
		Actions mouseActions = new Actions(driver);
		mouseActions.contextClick(element);
		// Imp Note: build.perform must be executed after each mouse action to perfrom the mouse operation
		mouseActions.build().perform();
	}
	
	public void mouseClickHold(WebElement element){
		
		// create an object from the Selenium "Actions" class
		Actions mouseActions = new Actions(driver);
		mouseActions.clickAndHold(element);
		// Imp Note: build.perform must be executed after each mouse action to perfrom the mouse operation
		mouseActions.build().perform();
	}
	
	public void mouseClickRelease(WebElement element){
		
		// create an object from the Selenium "Actions" class
		Actions mouseActions = new Actions(driver);
		mouseActions.release(element);
		// Imp Note: build.perform must be executed after each mouse action to perfrom the mouse operation
		mouseActions.build().perform();
	}
	
	public void mouseOver(WebElement element){
		
		// create an object from the Selenium "Actions" class
		Actions mouseActions = new Actions(driver);
		mouseActions.moveToElement(element);
		// Imp Note: build.perform must be executed after each mouse action to perfrom the mouse operation
		mouseActions.build().perform();
	}
		
	public void mouseClickDownArrow(WebElement element, WebDriver driver){
		
		element.sendKeys(Keys.ARROW_DOWN);
		
	}
	// After mouse click, moving the cursor to the required place in the right pane
	public void mouseRightClickDownArrow(){
		// create an object from the Selenium "Actions" class
		Actions mouseActions = new Actions(driver);
		//element.sendKeys(Keys.ARROW_DOWN);
		mouseActions.sendKeys(Keys.ARROW_DOWN);
		// Imp Note: build.perform must be executed after each mouse action to perfrom the mouse operation
		mouseActions.build().perform();
	}

}
