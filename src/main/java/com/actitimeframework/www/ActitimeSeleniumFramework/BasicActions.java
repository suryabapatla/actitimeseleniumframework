package com.actitimeframework.www.ActitimeSeleniumFramework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/* Methods to enter text, Select link/button/radio, getText, radio button: is selected,select and de-select check box,
select value from drop down, multiple select, , */

public class BasicActions extends Browsers {

	// enter text in edit box
	public void enterText(WebElement element, String textValue) {
		
		element.sendKeys(textValue);
	}

	//click on button/link/radio button
	public void clickElement(WebElement element) {
		element.click();
	}

	//get the text on the link or button name except the input text
	public String getText(WebElement element) {
		String elementText = element.getText();
		return elementText;
	}

	//radio button: is selected?
	public boolean statusRadioButton(WebElement element) {
		boolean elementStatus = element.isSelected();
		return elementStatus;
	}

	// select check box
	public void selectCheckBox(WebElement element) {
		if (element.isSelected()) {
		}
		else {
			element.click();
		}
	}

	// De-select check box
	public void deselectCheckBox(WebElement element) {
		if (element.isSelected()) {
			element.click();
		}
	}

	//select value from drop-down/list using visibleText, byIndex, byValue

	// by visible text
	public void selectByVisibleText(WebElement element, String visibleText){
		// creating an object to select the drop down element 
		Select listSelect = new Select(element);
		listSelect.selectByVisibleText(visibleText);
	}

	// by Index
	public void selectByIndex(WebElement element, int indexNum){
		// creating an object to select the dropdown element 
		Select listSelect = new Select(element);
		listSelect.selectByIndex(indexNum);
	}

	// by Value
	public void selectByValue(WebElement element, String listValue){
		// creating an object to select the dropdown element 
		Select listSelect = new Select(element);
		listSelect.selectByValue(listValue);
	}

	// multiple select by visible text, by index, by value
	// by visible text
	public void selectMultiVisibleTexts(WebElement element, String textsToBeSelected){
		// List item has: surya, bharath, rama, pramoda; I want to select bharath and pramoda
		// textsToBeSelected: bharath:pramoda:surya
		//creating an object to select the drop down element; converts the textsToBeSelected into array of strings [bharath, pramoda]
		String[] selectEachTextToBeSelected = textsToBeSelected.split(":");

		// creating an object to select the drop down (element will have dropdown property)
		Select listSelect = new Select(element);
		for (int i=0; i<selectEachTextToBeSelected.length; i++){
			listSelect.selectByVisibleText(selectEachTextToBeSelected[i]);
		}
	}

	// multiple select by index

	public void selectMultiByIndexes(WebElement element, String indexesToBeSelected){
		
		// creating an object to select the dropdown element (1:4:8)
		String[] selectEachIndexToBeSelected = indexesToBeSelected.split(":");
		//creating an object to select 
		Select listSelect = new Select(element);
		for (int i=0; i<selectEachIndexToBeSelected.length; i++){
			int indexNumber = Integer.parseInt(selectEachIndexToBeSelected[i]);
			listSelect.selectByIndex(indexNumber);
		}
	}

	// multiple select by value

	public void selectMultiByValues(WebElement element, String valuesToBeSelected){
		// creating an object to select the dropdown element 
		String[] selectEachValue = valuesToBeSelected.split(":");
		//creating an object to select 
		Select listSelect = new Select(element);
		for (int i=0; i<selectEachValue.length; i++){
			//int indexNumber = Integer.parseInt(selectEachIndexToBeSelected[i]);
			listSelect.selectByValue(selectEachValue[i]);
		}
	}

}
