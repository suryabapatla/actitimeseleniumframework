package com.actitimeframework.www.ActitimeSeleniumFramework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Synchronizers extends MouseActions{
	
		public void sleep(int timeinMilliSeconds) throws InterruptedException{
			Thread.sleep(timeinMilliSeconds);
		}
		//public void implicitWait(WebDriver driver,int timeInSeconds){
		public void implicitWait(int timeInSeconds){
			driver.manage().timeouts().implicitlyWait(timeInSeconds, TimeUnit.SECONDS);
		}
		//public String explictWait(String elementLocatorType,String elementLocatorValue,int timeInSeconds){
		public void explictWait(WebElement element, int timeInSeconds){
			WebDriverWait explictwait = new WebDriverWait(driver,timeInSeconds);
			//explictwait.until(ExpectedConditions.visibilityOf(elementLocator(driver,elementLocatorType, elementLocatorValue)));
			//explictwait.until(ExpectedConditions.visibilityOf(elementLocator(elementLocatorType, elementLocatorValue)));
			explictwait.until(ExpectedConditions.visibilityOf(element));
			
		}
				
		// Click event 
		public void explicitWaitElementClickable(WebElement element,int timeInSeconds) {
		    WebDriverWait explicitWaitElementClickable = new WebDriverWait(driver, timeInSeconds);
		    explicitWaitElementClickable.until(
		                        ExpectedConditions.elementToBeClickable(element));
		   
		}
	
	}


