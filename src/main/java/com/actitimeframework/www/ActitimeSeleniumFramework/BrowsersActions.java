package com.actitimeframework.www.ActitimeSeleniumFramework;

import java.util.Set;


public class BrowsersActions extends BasicActions{
   //public void openURL(WebDriver driver, String url) {
	
	public void openURL(String url) {
	   driver.get(url);
	}
   
   /*getCurrentURL, getPageTitle, getPageSource, getWindowHandle, getWindowHandles, 
   maximize, navigateForward, navigateBackward, browserRefresh, swithToTabs, browserQuit, browserClose*/
  // public String getCurrentURL(WebDriver driver){
	 public String getCurrentURL(){
		String currentURL = driver.getCurrentUrl();
		return currentURL;
	}
   
  // public String getPageTitle(WebDriver driver)	{
	   public String getPageTitle()	{
		String currentPageTitle = driver.getTitle();
		return currentPageTitle;
	}
   
   public String getPageSource(){
		String currentPageSource = driver.getPageSource();
		return currentPageSource;
	}
      
   public void browserMaximize(){
		driver.manage().window().maximize();
	}
   
   public void browserClose(){
		driver.close();
	}
	
   public void browserQuit(){
		driver.quit();
	}
	
   public void navigateBack(){
		driver.navigate().back();
	}
	
   public void browserRefresh()
	{
		driver.navigate().refresh();
	}
   
   public void navigateForward(){
		driver.navigate().forward();
	}
  
   public void switchToAlert(){
	   driver.switchTo().alert();
   }
   
   // getWindowHandle, Handles, switchToTabs 
   public String getWindowSession(){
	   String windowSession = driver.getWindowHandle();	
	   return windowSession;
   }
   
   // this method collects the multiple window handles that are open in single session 
   public Set<String> getWindowSessions(){
	   Set<String> windowSessions = driver.getWindowHandles();	
	   return windowSessions;
   }
   
   // switch to tab method gets focus on the required tab of multiple tabs in single session
 //navigate to window 
 	public void navigateToWindow(String expectedWindowTitle){
 	Set<String> windows=driver.getWindowHandles();
 	System.out.println(windows);
 	// for each loop; for each value of windows, this value is assigned to windowName and execute complete loop 
 	for(String windowName:windows)
 	{
 		System.out.println(windowName);
 		driver.switchTo().window(windowName);
 		System.out.println(getPageTitle());
 		//String actualTitle=getPageTitle(driver);
 		String actualTitle=getPageTitle();
 		if(expectedWindowTitle.equals(actualTitle))
 				{
 			break;
 				}
 	}
 		
 	}
   
}
   

