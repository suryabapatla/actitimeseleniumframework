package com.actitimeframework.www.ActitimeSeleniumFramework;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;

/* Element locators:
id
classname
xpath
css locator
name
tagname
linktext
partialtext*/
// Variable [WebDriver driver] is removed from each method parameters from all the classes except in Browsers class
// since this parameter is referred in Browsers method as global variable

public class ElementLocators extends BrowsersActions {
	//public WebElement elementLocator(WebDriver driver,String elementLocatorType,String elementLocatorValue)
	public WebElement elementLocator(String elementLocatorType,String elementLocatorValue)
	{
		elementLocatorType=elementLocatorType.toLowerCase();
		switch (elementLocatorType) {
			case "id":
				{
					WebElement element= driver.findElement(By.id(elementLocatorValue));
					return element;
				}

			case "class" :
				{
					WebElement element= driver.findElement(By.className(elementLocatorValue));
					return element;
				}
			
			case "xpath" :
				{
					WebElement element= driver.findElement(By.xpath(elementLocatorValue));
					return element;
				}
			
			case "cssLocator" :
				{
					WebElement element = driver.findElement(By.cssSelector(elementLocatorValue));
					return element;
				}
				
			case "name" : 
				{
					WebElement element = driver.findElement(By.name(elementLocatorValue));
					return element;
				}
				
			case "tagName" :
				{
					WebElement element = driver.findElement(By.tagName(elementLocatorValue));
					return element;
				}
			
			
			case "linktext" :
				{
					WebElement element= driver.findElement(By.linkText(elementLocatorValue));
					return element;
				}
				
			case "partialLinkText" : 
				{
					WebElement element = driver.findElement(By.partialLinkText(elementLocatorValue));
					return element; 
				}
			

			default:
				WebElement element=null;
				return element;
		}
	}

}
