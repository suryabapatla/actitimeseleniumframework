package com.actitimeframework.www.ActitimeSeleniumFramework;


import java.io.FileInputStream;
import java.io.FileOutputStream;
//import java.io.FileNotFoundException;
//import java.io.FilterOutputStream;
//import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class ExcelUtils extends ElementLocators {

	// read cell value
	public Object readExcelCell(String WorkBookName, String sheetName, int rowNum, int colNum) throws Exception {
		// location of the file along with file name  (fis will have the path of the file and excel file name) 
		FileInputStream fis = new FileInputStream(WorkBookName);
		XSSFWorkbook wBook = new XSSFWorkbook(fis);
		XSSFSheet wSheet = wBook.getSheet(sheetName);
		XSSFRow rowAddress = wSheet.getRow(rowNum);
		XSSFCell cellAddress = rowAddress.getCell(colNum);
		// to know the data type of cell (CellType has in 3.17 version of POI file in pom and not in older version 3.07
		CellType cellDataType = cellAddress.getCellTypeEnum();

		// if condition to find the cellDataType
		Object cellData = null; //this object is created when we do not know the data type of the cell
		if (cellDataType.equals(CellType.STRING)){
			cellData = cellAddress.getStringCellValue();
		}
		if (cellDataType.equals(CellType.BOOLEAN)){
			cellData = cellAddress.getBooleanCellValue();
		}
		if (cellDataType.equals(CellType.NUMERIC)){
			cellData = cellAddress.getNumericCellValue();
		}
		if (cellDataType.equals(CellType.FORMULA)){
			cellData = cellAddress.getCellFormula();
		}
		wBook.close();
		return cellData;
	}

	//write cell (void type since we are writing and no return)
	public void writeExcelCell(String WorkBookName, String sheetName, int rowNum, int colNum, Object cellValue) throws Exception {
		// location of the file along with file name  (fis will have the path of the file and excel file name) 
		FileInputStream fis = new FileInputStream(WorkBookName);
		XSSFWorkbook wBook = new XSSFWorkbook(fis);
		XSSFSheet wSheet = wBook.getSheet(sheetName);
		// if row and/or cell do not exist, create row/cell based on whether row exists or cell exist

		XSSFRow rowAddress; 
		try
		{
			rowAddress = wSheet.getRow(rowNum);	
		}
		catch (Exception e) {
			rowAddress = wSheet.createRow(rowNum);
		}

		XSSFCell cellAddress;
		try
		{
			cellAddress = rowAddress.getCell(colNum);
		}
		catch (Exception e) {
			cellAddress = rowAddress.createCell(colNum);
		}

		cellAddress.setCellValue(cellValue.toString());
		//after inserting the value in the cell, save the excel 
		FileOutputStream fos = new FileOutputStream(WorkBookName);
		wBook.write(fos);
		wBook.close();
	}

	// Read complete worksheet data *****
	// read cell value
	public Object[][] readExcelWorksheet(String WorkBookName, String sheetName) throws Exception {
		// reading the grid data (columns in each row should be same, then only the worksheet can be read)
		//location of the file along with file name  (fis will have the path of the file and excel file name) 
		FileInputStream fis = new FileInputStream(WorkBookName);
		XSSFWorkbook wBook = new XSSFWorkbook(fis);
		XSSFSheet wSheet = wBook.getSheet(sheetName);
		// find out number of rows in sheet by using getLastRowNum in worksheet (grid size) 
		int lastRowNum = wSheet.getLastRowNum();
		// get the first row and check the last cell number to find out the number of columns 
		XSSFRow row = wSheet.getRow(0);
		int lastColNum = row.getLastCellNum();
		// grid size completed with the above statements and now create the grid by creating array of object type
		Object[][] gridData = new Object[lastRowNum][lastColNum];
		// get data from the grid by creating row iterator first and later column iterator
		
		int rowNum = 0;
		int colNum = 0; 
		Iterator<Row> rowIterator =  wSheet.rowIterator();
		
		// skip the first row assuming the first row is header
		rowIterator.next();
		while (rowIterator.hasNext()) {
			// getting the row number/address
			Row rowAddress= rowIterator.next();
			// creating the cell iterator for each row 
			Iterator<Cell>  cellIterator = rowAddress.cellIterator();
			while (cellIterator.hasNext()){
				Cell cellAddress = cellIterator.next();
				// to know the data type of cell (CellType has in 3.17 version of POI file in pom and not in older version 3.07
				CellType cellDataType = cellAddress.getCellTypeEnum();

				// if condition to find the cellDataType and get the cell value
				Object cellData = null; //this object is created when we do not know the data type of the cell
				if (cellDataType.equals(CellType.STRING)){
					cellData = cellAddress.getStringCellValue();
				}
				if (cellDataType.equals(CellType.BOOLEAN)){
					cellData = cellAddress.getBooleanCellValue();
				}
				if (cellDataType.equals(CellType.NUMERIC)){
					cellData = cellAddress.getNumericCellValue();
				}
				if (cellDataType.equals(CellType.FORMULA)){
					cellData = cellAddress.getCellFormula();
				}
				
				gridData[rowNum][colNum] = cellData;
				colNum++;
			}
			
			colNum=0;
			rowNum++;
		}
		wBook.close();
		return gridData;

	}


}