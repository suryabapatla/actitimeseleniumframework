package com.actitimeframework.www.ActitimeSeleniumFramework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BaseTest extends Synchronizers{
	
	// extent reports changes 
	public static ExtentReports extent;
	public static ExtentTest test;

	// @Test, @Before, @After, @BeforeSuite, @BeforeTest, @AfterTest, @BeforeClass
	// Upadating the log4j propeties configuration of file location and file name
	// log4j.properties file was placed in the src/main/resources/log4j.properties
	private void updateLog4jConfiguration(String logFile) { 
		
		Properties props = new Properties(); 
		try { 
			InputStream configStream = getClass().getResourceAsStream( "/log4j.properties"); 
			props.load(configStream); 
			configStream.close(); 
		} catch (IOException e) { 
			System.out.println("Error configuration file not loaded"); 
		} 
		//
		props.setProperty("log4j.appender.file.File", logFile); 
		LogManager.resetConfiguration(); 
		PropertyConfigurator.configure(props); 
	}
	
	@BeforeSuite (groups= {"Smoke", "Regression"})

	public void updateLog4jLogfileConfig() throws IOException
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss");  
		LocalDateTime localDateTime = LocalDateTime.now();  
		dtf.format(localDateTime);
		// updating the format and location of the log file 
		updateLog4jConfiguration("src/logs/Actitime_"+ (dtf.format(localDateTime))+".log");
		//extent reports changes
		extent = new ExtentReports("src/ExtentReports/ExtentReportResults_" + (dtf.format(localDateTime))+".html", true);
		extent.loadConfig(new File("src/main/resources/extent-config.xml"));
		
	}
	@BeforeMethod (groups= {"Smoke", "Regression"})
	public void beforeEach(Method method) {
		//test = extent.startTest((new Object(){}.getClass().getEnclosingMethod().getName()));
		
		test = extent.startTest(method.getName());
		openBrowser("chrome");
		implicitWait(15);
		test.log(LogStatus.PASS, "Browser is opened");
		//openURL("https://www.google.com");
	}

	@AfterMethod (groups= {"Smoke", "Regression"})
	public void afterEach() {
		browserQuit();
		test.log(LogStatus.PASS, "browser session is closed");
		extent.endTest(test);
	}

	@AfterSuite (groups= {"Smoke", "Regression"})
	public void afterSuite(){
		extent.flush();
		extent.close();
	}

}
