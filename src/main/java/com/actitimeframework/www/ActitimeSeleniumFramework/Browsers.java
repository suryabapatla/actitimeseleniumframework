package com.actitimeframework.www.ActitimeSeleniumFramework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Browsers {
	public static WebDriver driver = null;

	public void openBrowser(String browserType) {
		switch (browserType.toLowerCase()) {
		case "ie":
			System.setProperty("webdriver.ie.driver", "src/helpers/Drivers/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			break;
		case "microsoftEdge":
			System.setProperty("webdriver.edge.driver", "src/helpers/Drivers/MicrosoftWebDriver.exe");
			driver = new EdgeDriver();
			break;
		case "chrome":
			System.setProperty("webdriver.chrome.driver", "src/helpers/Drivers/chromedriver.exe");
			driver = new ChromeDriver();
			break;
		case "fireFox":
			System.setProperty("webdriver.gecko.driver", "src/helpers/Drivers/geckodriver.exe");
			driver = new FirefoxDriver();
			break;
		}
	}
}




